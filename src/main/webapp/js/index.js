$(document).ready(function() {

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    initChart("EURUSD");
    initChart("USDJPY");

    // init buttons
    $("#startRobotButton").click(function() {
        $("#workerStatus").text($.ajax({type: "GET",
            url: "http://" + location.host + "/" + location.pathname.split('/')[1] + "/ctrl?what=start",
            async: true
        }).responseText);
    });
    $("#stopRobotButton").click(function() {
        $("#workerStatus").text($.ajax({type: "GET",
            url: "http://" + location.host + "/" + location.pathname.split('/')[1] + "/ctrl?what=stop",
            async: true
        }).responseText);

    });

});

