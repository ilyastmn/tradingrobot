function initChart(symbol) {
    var chart;
    $('#container_' + symbol).highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function() {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var secondSeries = this.series[1];
                    var markovSeries = this.series[2];
                    setInterval(function() {
                        var x = (new Date()).getTime();
                        $.getJSON("http://" + location.host + "/" + location.pathname.split('/')[1] + "/info?sym=" + symbol, function(json) {
                            series.addPoint([x, json.latest.bid], true, true);
                            secondSeries.addPoint([x, json.kalman], true, true);
                            markovSeries.addPoint([x, json.markov], true, true);
                        });
                    }, 1000);
                }
            }
        },
        title: {
            text: symbol
        },
        xAxis: {
            type: 'datetime',
            //tickPixelInterval: 150,
            tickInterval: 24 * 3600 * 1000,
            tickWidth: 1,
            minPadding: 0.1,
            maxPadding: 0.1
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
                name: symbol,
                data: (function() {
                    // generate an array of random data
                    var data = [],
                            time = (new Date()).getTime(),
                            i;

                    for (i = -5; i <= 0; i++) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                })()
            }, {
                name: "kalman filter",
                data: (function() {
                    // generate an array of random data
                    var data = [],
                            time = (new Date()).getTime(),
                            i;

                    for (i = -5; i <= 0; i++) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                })()
            }, {
                name: "markov chain",
                data: (function() {
                    // generate an array of random data
                    var data = [],
                            time = (new Date()).getTime(),
                            i;

                    for (i = -5; i <= 0; i++) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                })()
            }]
    });
}
