/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.web;

import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.hmm.AnalyzeStoredRatesThread;
import com.webrobot.stats.hmm.BuyByKalmanThread;
import com.webrobot.stats.model.Rate;
import com.webrobot.stats.worker.Worker;

/**
 *
 * @author Ilyas
 */
public class ServiceUtil {

    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    private URL serviceUrl;
    private static volatile ServiceUtil instance;

    public static ServiceUtil getInstance() {
        ServiceUtil localInstance = instance;
        if (localInstance == null) {
            synchronized (Worker.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ServiceUtil();
                }
            }
        }
        return localInstance;
    }

    private ServiceUtil() {
        try {
            serviceUrl = new URL("http://rates.fxcm.com/RatesXML");
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (Exception ex) {
            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void callService() throws Exception {
//        System.setProperty("http.proxyHost", "localhost");
//        System.setProperty("http.proxyPort", "3128");
        long startTime = System.currentTimeMillis();
        InputStream is = serviceUrl.openConnection().getInputStream();
        Document doc = dBuilder.parse(is);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("Rate");
        System.out.println(nList.getLength());
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Rate r = new Rate((Element) nNode);
                RoboContext.addRateToContainer(r);
                RoboContext.updateLatestRate(r.getSymbol(), r);
                
                new BuyByKalmanThread(r.getSymbol()).start();
                
                new AnalyzeStoredRatesThread(r);
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println(" Service call execution time : " + (endTime - startTime));
    }
}
