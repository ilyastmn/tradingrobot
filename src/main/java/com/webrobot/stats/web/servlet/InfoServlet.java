/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.web.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webrobot.stats.context.FinanceContext;
import com.webrobot.stats.context.KalmanContext;
import com.webrobot.stats.context.MarkovContext;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.model.KalmanWrapper;
import com.webrobot.stats.model.Rate;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.activation.MimeType;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ilyas
 */
@WebServlet(name = "InfoServlet", urlPatterns = {"/info"})
public class InfoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        try {
            String sym = request.getParameter("sym");
            String amount = request.getParameter("amount");
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            if (sym == null) {
                out.write(gson.toJson(RoboContext.getLatestRate()));
            } else {
                KalmanWrapper wrapper = KalmanContext.filteredRates.get(sym);

                Map<String, Object> rates = new HashMap<>();
                rates.put("latest", RoboContext.getLatestRate().get(sym));
                rates.put("kalman", wrapper.getFilter().getX() == null ? 0D : wrapper.getFilter().getX().get(1, 0));
                rates.put("markov", MarkovContext.getPredictionBySymbol(sym) == null ? 0D : MarkovContext.getPredictionBySymbol(sym).getPrice());

                out.write(gson.toJson(rates));
            }

            if (amount != null && amount.equals("wallet")) {
                FinanceContext asd;
                Map<String, Object> info = new HashMap<>();
                info.put("wallet", FinanceContext.wallet);
                info.put("boughtRates", FinanceContext.boughtStock);
                out.write(gson.toJson(info));
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
