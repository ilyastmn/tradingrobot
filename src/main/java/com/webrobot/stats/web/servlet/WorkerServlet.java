package com.webrobot.stats.web.servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.webrobot.stats.worker.Worker;
import com.webrobot.stats.worker.WorkerLocator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ilyas
 */
@WebServlet(urlPatterns = {"/ctrl"})
public class WorkerServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson json = new GsonBuilder().setPrettyPrinting().create();
        String op = request.getParameter("what");
        Worker worker = WorkerLocator.getWorkerInstance();
//        Worker flusher = WorkerLocator.getFlusherInstance();
        Worker markov = WorkerLocator.getMarkovInstance();
        Worker kalman = WorkerLocator.getKalmanInstance();

        if (op != null && !op.isEmpty()) {
            if (op.equals("start")) {
                //URL: stats/ctrl?what=start
                worker.start();
                out.write("worker is running : " + worker.isRunning());
//                flusher.start();
//                out.write("flusher is running : " + flusher.isRunning());
                markov.start();
                out.write("markov is running : " + markov.isRunning());

                kalman.start();
                out.write(" kalman is running : " + kalman.isRunning());

            }
            if (op.equals("stop")) {
                //URL: stats/ctrl?what=stop
                worker.stop();
                out.write("worker is stopped : " + worker.isRunning());
//                flusher.stop();
//                out.write("flusher is stopped : " + flusher.isRunning());
                markov.stop();
                out.write("markov is stopped : " + markov.isRunning());

                kalman.stop();
                out.write("markov is stopped : " + kalman.isRunning());
            }
            if (op.equals("info")) {
                //URL: stats/ctrl?what=info
                Map map = new HashMap<String, String>();
                map.put("worker", worker.isRunning());
//                map.put("flusher", flusher.isRunning());
                map.put("markov", markov.isRunning());
                map.put("kalman", kalman.isRunning());
                out.write(json.toJson(map));
            }

        }

        out.flush();
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
