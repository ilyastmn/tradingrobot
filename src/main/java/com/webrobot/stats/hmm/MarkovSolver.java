/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.hmm;

import com.webrobot.stats.context.MarkovContext;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.model.Rate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author kilrwhle
 */
public class MarkovSolver {

    /**
     * Updates alphabet
     */
    public static void refreshAlphabet() {
        //copy to local var
        Map<String, List<Rate>> cache = new HashMap<>(MarkovContext.getMarkovCache());
        List<Rate> temp;
        int listSize = 0;
        for (String symbol : cache.keySet()) {
            temp = cache.get(symbol);
            listSize = temp.size();

            if (listSize < 2) {
                continue;
            }

            for (int i = 1; i < listSize; i++) {
                MarkovContext.pushValue(
                        symbol,
                        temp.get(i - 1).getBid(),
                        temp.get(i).getBid());
            }
        }

        MarkovContext.resetCache();
    }
    
    /**
     * Predicts resulting price
     */
    public static void predictPrice() {
        //copy to local var
        Map<String, Set<Occurence>> occs = new HashMap<>(MarkovContext.getOccurences());
        Rate latest;
        double predicted;
        for(String s : occs.keySet()) {
            latest = RoboContext.getLatestRateForSymbol(s);
            predicted = findLetter(s, occs.get(s), latest.getBid());
            MarkovContext.updatePrediction(s, predicted);
        }
    }
    
    /**
     * Finds a 'letter' in alphabet; if fails, searches for closest value in occurences
     * 
     * @param symbol
     * @param occurences
     * @param bidPrice
     * @return 
     */
    public static double findLetter(String symbol, Set<Occurence> occurences, double bidPrice) {
        Occurence temp = null;
        //search for letter
        for(Occurence o : occurences) {
            if(o.isInFuzzyRange(bidPrice)) {
                temp = o;
                break;
            }
        }
        if(temp != null) {
            return temp.getResultingPrice();
        }
        //letter not found, get closest value
        temp = MarkovContext.getCandidateClosest(symbol, bidPrice);
        if(temp != null) {
            return temp.getResultingPrice();
        }
        return 0D;
    }
}
