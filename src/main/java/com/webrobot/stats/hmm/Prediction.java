/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.hmm;

import java.util.Objects;

/**
 *
 * @author kilrwhle
 */
public class Prediction {

    private String symbol;
    private double price;
    private long hitCount;

    public Prediction(String symbol, double price) {
        this.symbol = symbol;
        this.price = price;
    }

    public void hit() {
        this.hitCount++;
    }

    @Override
    public String toString() {
        return "Prediction{" + "symbol=" + symbol + ", price=" + price + ", hitCount=" + hitCount + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.symbol);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prediction other = (Prediction) obj;
        if (!Objects.equals(this.symbol, other.symbol)) {
            return false;
        }
        return true;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getHitCount() {
        return hitCount;
    }

    public void setHitCount(long hitCount) {
        this.hitCount = hitCount;
    }
}
