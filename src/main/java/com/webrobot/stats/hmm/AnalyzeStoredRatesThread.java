/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.hmm;

import com.webrobot.stats.context.FinanceContext;
import com.webrobot.stats.model.Rate;

/**
 *
 * @author falcon
 */
public class AnalyzeStoredRatesThread extends Thread {

    Rate rate;

    public AnalyzeStoredRatesThread(Rate rate) {
        this.rate = rate;
    }

    @Override
    public void run() {
        FinanceContext.tryToSellRate(rate);
    }
}
