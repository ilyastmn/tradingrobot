/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.hmm;

import java.util.Objects;

/**
 * The element of probability matrix for Markov alg
 *
 * @author aishmanov
 */
public class Occurence {

    public static final double FUZZY_COEFFICIENT = 0.00001D;
    private double previousPrice;
    private double resultingPrice;
    private long occurences = 1L;
    private String symbol;

    public Occurence(double previousPrice, double resultingPrice, String symbol) {
        this.previousPrice = previousPrice;
        this.resultingPrice = resultingPrice;
        this.symbol = symbol;
    }

    /**
     * Increment occurences number
     */
    public void increment() {
        this.occurences++;
    }

    /**
     * Checks if price is approximately equal to previous price
     *
     * @param value
     * @return
     */
    public boolean isInFuzzyRange(double value) {
        return Math.abs(value - previousPrice) < FUZZY_COEFFICIENT;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.previousPrice) ^ (Double.doubleToLongBits(this.previousPrice) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.resultingPrice) ^ (Double.doubleToLongBits(this.resultingPrice) >>> 32));
        hash = 79 * hash + Objects.hashCode(this.symbol);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Occurence other = (Occurence) obj;
        if (!Objects.equals(this.symbol, other.symbol)) {
            return false;
        }
        //test for fuzzy accessory
        return isInFuzzyRange(other.previousPrice);
    }

    @Override
    public String toString() {
        return "Occurence {" + symbol
                + ", count: " + occurences
                + ", past: " + previousPrice
                + ", current: " + resultingPrice + "}";
    }

    public double getResultingPrice() {
        return resultingPrice;
    }

    public void setResultingPrice(double resultingPrice) {
        this.resultingPrice = resultingPrice;
    }

    public double getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(double previousPrice) {
        this.previousPrice = previousPrice;
    }

    public long getOccurences() {
        return occurences;
    }

    public void setOccurences(long occurences) {
        this.occurences = occurences;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
