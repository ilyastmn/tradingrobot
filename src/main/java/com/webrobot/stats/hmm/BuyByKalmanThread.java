/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.hmm;

import com.webrobot.stats.context.FinanceContext;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.model.Rate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author falcon
 */
public class BuyByKalmanThread extends Thread {

    String currency;

    public BuyByKalmanThread(String currency) {
        this.currency = currency;
    }

    @Override
    public void run() {
        int counter = 0;

        List<Rate> rates = RoboContext.getRatesContainer().get(currency);
        if (rates != null && rates.size() > 20) {
            for (int i = rates.size() - 6; i < rates.size(); i++) {
                if (rates.get(i).getBid() > rates.get(i - 1).getBid()) {
                    counter++;
                }
            }

            if (counter > 3) {
                try {
                    FinanceContext.buyRate(rates.get(rates.size() - 1), 1);
                } catch (Exception ex) {
                    System.out.println("WALLET IS FUCKING EMPTY");
                }
            }
        }
    }
}
