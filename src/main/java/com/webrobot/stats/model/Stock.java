/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.model;

/**
 *
 * @author Ilyas
 */
public class Stock {

    private Rate rate;
    private int amount;
    private double price;

    public Stock() {
    }

    public Stock(Rate rate, int amount) {
        this.rate = rate;
        this.amount = amount;
        price = rate.getBid() * amount;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.rate != null ? this.rate.hashCode() : 0);
        hash = 89 * hash + this.amount;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stock other = (Stock) obj;
        if (this.rate != other.rate && (this.rate == null || !this.rate.equals(other.rate))) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }
        return true;
    }
}
