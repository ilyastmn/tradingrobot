/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.model;

import com.microsoft.windowsazure.services.table.client.TableServiceEntity;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.w3c.dom.Element;
import com.webrobot.stats.context.Helper;

/**
 *
 * @author Ilyas
 */
public class Rate extends TableServiceEntity implements Asset, Comparable<Rate> {

    //TableServiceEntity constructor (part key + row key)
    public Rate(String symbol, Date last) {
        this.partitionKey = symbol; //partitionKey is asset code
        this.rowKey = (last.getTime() + ""); //store ms in order to re-convert later
    }
    private double bid;
    private double ask;
    private double high;
    private double low;
    private int direction;

    public Rate() {

    }

    public Rate(String s) {
        bid = 0.0;
        ask = 0.0;
        high = 0.0;
        low = 0.0;
        direction = 1;
        if (s.equals("n")) {
            bid = 10.0;
            ask = 10.0;
            high = 10.0;
            low = 10.0;
            direction = 1;
            partitionKey = "USDKZT";
        }
    }

    public Rate(Element xmlElement) {

        this.partitionKey = xmlElement.getAttribute("Symbol");

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            Date d = formatter.parse(xmlElement.getElementsByTagName("Last").item(0).getTextContent());
            this.rowKey = (d.getTime() + "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        this.ask = Double.valueOf(xmlElement.getElementsByTagName("Ask").item(0).getTextContent());
        this.bid = Double.valueOf(xmlElement.getElementsByTagName("Bid").item(0).getTextContent());
        this.high = Double.valueOf(xmlElement.getElementsByTagName("High").item(0).getTextContent());
        this.low = Double.valueOf(xmlElement.getElementsByTagName("Low").item(0).getTextContent());
        this.direction = Integer.valueOf(xmlElement.getElementsByTagName("Direction").item(0).getTextContent());
    }

    @Override
    public int compareTo(Rate o) {
        return this.getLast().compareTo(o.getLast());
    }

    public String getSymbol() {
        return this.partitionKey;
    }

    public void setSymbol(String symbol) {
        this.partitionKey = symbol;
    }

    public Date getLast() {
        if (this.getRowKey() == null) {
            return null;
        } else {
            return new Date(Long.valueOf(this.getRowKey()));
        }
    }

    public void setLast(Date last) {
        if (last == null) {
            this.rowKey = null;
        } else {
            this.rowKey = (last.getTime() + "");
        }
    }

    @Override
    public String toString() {
        return "Rate: " + getSymbol() + ", date: "
                + Helper.dateTimeToString(getLast()) + ", bid: "
                + getBid() + ", ask: " + getAsk() + ", high: " + getHigh()
                + ", low: " + getLow() + ", direction: " + getDirection();
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.bid) ^ (Double.doubleToLongBits(this.bid) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.ask) ^ (Double.doubleToLongBits(this.ask) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.high) ^ (Double.doubleToLongBits(this.high) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.low) ^ (Double.doubleToLongBits(this.low) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.direction) ^ (Double.doubleToLongBits(this.direction) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rate other = (Rate) obj;
        if (Double.doubleToLongBits(this.bid) != Double.doubleToLongBits(other.bid)) {
            return false;
        }
        if (Double.doubleToLongBits(this.ask) != Double.doubleToLongBits(other.ask)) {
            return false;
        }
        if (Double.doubleToLongBits(this.high) != Double.doubleToLongBits(other.high)) {
            return false;
        }
        if (Double.doubleToLongBits(this.low) != Double.doubleToLongBits(other.low)) {
            return false;
        }
        if (this.direction != other.direction) {
            return false;
        }
        return true;
    }
}
