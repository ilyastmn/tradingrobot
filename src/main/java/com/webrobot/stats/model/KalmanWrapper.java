/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.model;

import com.webrobot.stats.hmm.KalmanFilter;
import java.util.Objects;

/**
 * Each rate has own kalman filter
 *
 * @author falcon
 */
public class KalmanWrapper {

    public KalmanFilter filter;
    public Rate rate;

    public KalmanWrapper() {
    }

    public KalmanWrapper(KalmanFilter filter, Rate rate) {
        this.filter = filter;
        this.rate = rate;
    }

    public KalmanFilter getFilter() {
        return filter;
    }

    public void setFilter(KalmanFilter filter) {
        this.filter = filter;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.filter);
        hash = 31 * hash + Objects.hashCode(this.rate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KalmanWrapper other = (KalmanWrapper) obj;
        if (!Objects.equals(this.filter, other.filter)) {
            return false;
        }
        if (!Objects.equals(this.rate, other.rate)) {
            return false;
        }
        return true;
    }
}