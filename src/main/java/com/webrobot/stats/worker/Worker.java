package com.webrobot.stats.worker;

/**
 *
 * @author Ilyas
 */
public interface Worker {

    public void start();

    public void stop();

    public boolean isRunning();

    public void killTimer();
}
