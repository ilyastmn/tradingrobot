package com.webrobot.stats.worker;

import Jama.Matrix;
import com.webrobot.stats.context.KalmanContext;
import java.util.Timer;
import java.util.TimerTask;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.hmm.KalmanFilter;
import com.webrobot.stats.model.KalmanWrapper;
import com.webrobot.stats.model.Rate;
import com.webrobot.stats.web.ServiceUtil;
import static java.lang.Math.pow;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ilyas
 */
public class KalmanWorker extends TimerTask implements Worker {

    public static final int KALMAN_INTERVAL_SEC = 2; //Once in 2 seconds
    private static Timer timer = new Timer(true);
    private static boolean running = false;

    @Override
    public void start() {
        if (!running) {
            running = true;
            try {
                timer.scheduleAtFixedRate(this, 0, KALMAN_INTERVAL_SEC * 1000);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        try {
            if (running) {
                processData();
            } else {
                System.out.println("Kalman thread is running but not processing any data");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public void killTimer() {
        timer.purge();
        timer.cancel();
    }

    private void processData() {

        Map<String, Rate> map = RoboContext.getLatestRate();
        for (Map.Entry<String, Rate> entry : map.entrySet()) {
            if (!KalmanContext.filteredRates.containsKey(entry.getKey())) {
                //process parameters
                double dt = 50d;
                double processNoiseStdev = 3;
                double measurementNoiseStdev = 5000;
                double m = 0;

                KalmanFilter filter = KalmanFilter.buildKF(0, 0, dt, pow(processNoiseStdev, 2) / 2, pow(measurementNoiseStdev, 2));
                filter.setX(new Matrix(new double[][]{{(double) WorkerImpl.getCounter()}, {0}, {entry.getValue().getBid()}, {0}}));
                KalmanWrapper wrapper = new KalmanWrapper(filter, entry.getValue());
                KalmanContext.filteredRates.put(entry.getKey(), wrapper);
            } else {
                KalmanWrapper wrapper = KalmanContext.filteredRates.get(entry.getKey());
                KalmanFilter filter = wrapper.getFilter();
                filter.predict();
                filter.correct(new Matrix(new double[][]{{WorkerImpl.getCounter()}, {entry.getValue().getBid()}}));
            }
        }
    }
}
