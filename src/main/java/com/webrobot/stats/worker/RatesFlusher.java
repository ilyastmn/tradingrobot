/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.model.Rate;
import com.webrobot.stats.persistence.AzureTableStorage;

/**
 *
 * @author kilrwhle
 */
public class RatesFlusher extends TimerTask implements Worker {

    public static final int FLUSH_INTERVAL_SEC = 600;   //once in 10 minutes
    private static Timer timer = new Timer(true);
    private static boolean running = false;

    //writes all rates to storage
    @Override
    public void run() {
        try {
            if (running) {

                List<Map<String, List<Rate>>> archive = new ArrayList<Map<String, List<Rate>>>(RoboContext.getArchive());
                if (archive.isEmpty()) {
                    return;
                } else {
                    RoboContext.flush(); //clear archive
                }

                AzureTableStorage stor = AzureTableStorage.getInstance();
                for (Map<String, List<Rate>> map : archive) {
                    for (List<Rate> rates : map.values()) {
                        stor.batchInsert(rates, AzureTableStorage.TABLE_RATES);
                    }
                }
            } else {
                System.out.println("rates flusher is not working");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public void start() {
        if (!running) {
            running = true;
            timer.scheduleAtFixedRate(this, 0, FLUSH_INTERVAL_SEC * 1000);
        }
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public void killTimer() {
        timer.purge();
        timer.cancel();
    }
}
