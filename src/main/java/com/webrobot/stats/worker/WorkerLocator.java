/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.worker;

/**
 *
 * @author Ilyas
 */
public class WorkerLocator {

    private static volatile Worker worker;
    private static volatile Worker flusher;
    private static volatile Worker markov;
    private static volatile Worker kalman;

    public static Worker getWorkerInstance() {
        Worker localInstance = worker;
        if (localInstance == null) {
            synchronized (Worker.class) {
                localInstance = worker;
                if (localInstance == null) {
                    worker = localInstance = new WorkerImpl();
                }
            }
        }
        return localInstance;
    }

    public static Worker getFlusherInstance() {
        Worker localInstance = flusher;
        if (localInstance == null) {
            synchronized (Worker.class) {
                localInstance = flusher;
                if (localInstance == null) {
                    flusher = localInstance = new RatesFlusher();
                }
            }
        }
        return localInstance;
    }

    public static Worker getMarkovInstance() {
        Worker localInstance = markov;
        if (localInstance == null) {
            synchronized (Worker.class) {
                localInstance = markov;
                if (localInstance == null) {
                    markov = localInstance = new MarkovWorker();
                }
            }
        }
        return localInstance;
    }

    public static Worker getKalmanInstance() {
        Worker localInstance = kalman;
        if (localInstance == null) {
            synchronized (KalmanWorker.class) {
                localInstance = kalman;
                if (localInstance == null) {
                    kalman = localInstance = new KalmanWorker();
                }
            }
        }
        return localInstance;
    }
}
