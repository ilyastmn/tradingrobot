/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.worker;

import com.webrobot.stats.context.MarkovContext;
import com.webrobot.stats.hmm.MarkovSolver;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author aishmanov
 */
public class MarkovWorker extends TimerTask implements Worker {

    public static final int MARKOV_INTERVAL_SEC = 2;   //once in 1 minute
    private static Timer timer = new Timer(true);
    private static boolean running = false;

    @Override
    public void run() {
        try {
            if (running) {

                MarkovSolver.refreshAlphabet();
                MarkovSolver.predictPrice();

//                System.out.println("=======================================");
////                for(String s : MarkovContext.getOccurences().keySet()) {
////                    System.out.println(s);
////                    for(Occurence o : MarkovContext.getOccurences().get(s)) {
////                        System.out.println("\t" + o.toString());
////                    }
////                }
//                System.out.println(MarkovContext.getOccurences().get("EURUSD"));
//                System.out.println(MarkovContext.getOccurences().get("USDJPY"));
//                System.out.println("=======================================");
//                
//                System.out.println("=======================================");
////                System.out.println(MarkovContext.getPredictions().size());
////                for(String s : MarkovContext.getPredictions().keySet()) {
////                    System.out.println(s + ", " + MarkovContext.getPredictions().get(s).getPrice());
////                }
//                System.out.println(MarkovContext.getPredictions().get("EURUSD"));
//                System.out.println(MarkovContext.getPredictions().get("USDJPY"));
//                System.out.println("=======================================");

            } else {
                System.out.println("markov solver is not working");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void start() {
        if (!running) {
            running = true;
            timer.scheduleAtFixedRate(this, 0, MARKOV_INTERVAL_SEC * 1000);
        }
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void killTimer() {
        timer.purge();
        timer.cancel();
    }
}
