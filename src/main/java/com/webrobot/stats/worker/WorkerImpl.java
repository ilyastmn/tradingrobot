package com.webrobot.stats.worker;

import java.util.Timer;
import java.util.TimerTask;
import com.webrobot.stats.context.RoboContext;
import com.webrobot.stats.web.ServiceUtil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ilyas
 */
public class WorkerImpl extends TimerTask implements Worker {

    public static final int ARCHIVE_INTERVAL_ITER = 2; //Once in 30 iterations
    public static final int WORKER_INTERVAL_SEC = 2; //Once in 2 seconds
    private static Timer timer = new Timer(true);
    private static boolean running = false;
    private static int counter = 0;

    @Override
    public void start() {
        if (!running) {
            running = true;
            try {
                timer.scheduleAtFixedRate(this, 0, WORKER_INTERVAL_SEC * 1000);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        try {
            if (running) {
                counter++;
                ServiceUtil.getInstance().callService();

                if (counter % ARCHIVE_INTERVAL_ITER == 0) {
                    counter = 0;
                   RoboContext.archiveRates();
                }
            } else {
                System.out.println("task is not sending any requests");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public void killTimer() {
        timer.purge();
        timer.cancel();
    }

    public static int getCounter() {
        return counter;
    }
}
