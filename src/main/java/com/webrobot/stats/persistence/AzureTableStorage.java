/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.persistence;

import com.microsoft.windowsazure.services.core.storage.CloudStorageAccount;
import com.microsoft.windowsazure.services.core.storage.StorageException;
import com.microsoft.windowsazure.services.table.client.CloudTable;
import com.microsoft.windowsazure.services.table.client.CloudTableClient;
import com.microsoft.windowsazure.services.table.client.TableBatchOperation;
import com.microsoft.windowsazure.services.table.client.TableConstants;
import com.microsoft.windowsazure.services.table.client.TableOperation;
import com.microsoft.windowsazure.services.table.client.TableQuery;
import com.microsoft.windowsazure.services.table.client.TableResult;
import com.microsoft.windowsazure.services.table.client.TableServiceEntity;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kilrwhle
 */
public class AzureTableStorage {

    public static final String STORAGE_ACCOUNT = "kilrwhle";
    public static final String STORAGE_KEY = "FKD3PELJLdlFn461NNsyQ4PQBDyWTB/LFj5uPVQhR2XsmfxgTjnMzOUCIpFrNmQ0p1zPY71Oyeg631eBdjNE5Q==";
    public static final String CONNECTION_STRING = "DefaultEndpointsProtocol=http;AccountName=" + STORAGE_ACCOUNT + ";AccountKey=" + STORAGE_KEY;
    private static CloudStorageAccount storageAccount;
    private static CloudTableClient tableClient;
    private static AzureTableStorage instance;
    //tables
    public static final String TABLE_RATES = "rates";
    public static final String TABLE_USERS = "users";
    private static CloudTable rates;
    private static CloudTable users;

    public static AzureTableStorage getInstance() {
        if (instance == null) {
            instance = new AzureTableStorage();
        }
        return instance;
    }

    private AzureTableStorage() {
        try {
            initClient();
            initTables();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        } catch (StorageException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Creates azure table client
     *
     * @throws URISyntaxException
     * @throws InvalidKeyException
     */
    private void initClient() throws URISyntaxException, InvalidKeyException {
        // Retrieve storage account from connection-string
        storageAccount = CloudStorageAccount.parse(CONNECTION_STRING);
        // Create the table client.
        tableClient = storageAccount.createCloudTableClient();
    }

    /**
     * Creates tables if not exist
     *
     * @throws URISyntaxException
     * @throws StorageException
     * @throws NullPointerException
     */
    private void initTables() throws URISyntaxException, StorageException, NullPointerException {
        users = tableClient.getTableReference(TABLE_USERS);
        users.createIfNotExist();
        rates = tableClient.getTableReference(TABLE_RATES);
        rates.createIfNotExist();
    }

    /**
     * Insert operation
     *
     * @param <E>
     * @param entity
     * @param tableName
     * @return 0 if HTTP 2xx returned by azure
     */
    public <E extends TableServiceEntity> int insert(E entity, String tableName) {
        TableOperation insert = TableOperation.insert(entity);
        try {
            TableResult result = tableClient.execute(tableName, insert);
            if (result.getHttpStatusCode() >= 200 && result.getHttpStatusCode() < 300) {
                return 0;
            } else {
                return result.getHttpStatusCode();
            }
        } catch (StorageException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    /**
     * Batch insert operation
     *
     * @param <E>
     * @param entities
     * @param tableName
     * @return 0 if all results are HTTP 2xx
     */
    public <E extends TableServiceEntity> int batchInsert(List<E> entities, String tableName) {
        if (entities == null || entities.isEmpty()) {
            return -1;
        }

        TableBatchOperation batchInsert = new TableBatchOperation();
        for (E e : entities) {
            batchInsert.insert(e);
        }
        try {
            List<TableResult> results = tableClient.execute(tableName, batchInsert);
            int errCounter = 0;
            for (TableResult res : results) {
                if (res.getHttpStatusCode() < 200 || res.getHttpStatusCode() >= 300) {
                    errCounter++;
                }
            }
            return errCounter;
        } catch (StorageException ex) {
//            ex.printStackTrace();
            return -1;
        }
    }

    /**
     * Select entities by partition key
     *
     * @param <E>
     * @param partitionKey
     * @param tableName
     * @param entityClass
     * @return List of entities with specified PK
     */
    public <E extends TableServiceEntity> List<E> selectByPK(String partitionKey, String tableName, Class entityClass) {
        // Create a filter condition where the partition key is partitionKey
        String partitionFilter = TableQuery.generateFilterCondition(
                TableConstants.PARTITION_KEY,
                TableQuery.QueryComparisons.EQUAL,
                partitionKey);

        // Specify a partition query and apply filter
        TableQuery<E> partitionQuery = TableQuery.from(tableName, entityClass)
                .where(partitionFilter);

        // Execute select
        Iterable<E> it = tableClient.execute(partitionQuery);

        // Copy results in a list
        List<E> copy = new ArrayList<E>();
        while (it.iterator().hasNext()) {
            copy.add(it.iterator().next());
        }
        return copy;
    }
}
