/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author aishmanov
 */
public class Helper {

    public static final SimpleDateFormat date = new SimpleDateFormat("dd.mm.yyyy");
    public static final SimpleDateFormat dateTime = new SimpleDateFormat("dd.mm.yyyy HH:MM:ss");

    public static String dateToString(Date d) {
        try {
            return date.format(d);
        } catch (Exception ex) {
            return "";
        }
    }

    public static String dateTimeToString(Date d) {
        try {
            return dateTime.format(d);
        } catch (Exception ex) {
            return "";
        }
    }

    public static Date dateStringToDate(String s) {
        try {
            return date.parse(s);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Date dateTimeStringToDate(String s) {
        try {
            return dateTime.parse(s);
        } catch (Exception ex) {
            return null;
        }
    }
}
