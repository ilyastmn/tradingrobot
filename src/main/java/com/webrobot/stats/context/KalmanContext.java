/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import com.webrobot.stats.hmm.KalmanFilter;
import com.webrobot.stats.model.KalmanWrapper;
import com.webrobot.stats.model.Rate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author falcon
 */
public class KalmanContext {

    public static Map<String, KalmanWrapper> filteredRates;

    static {
        filteredRates = Collections.synchronizedMap(new HashMap<String, KalmanWrapper>());
    }
}
