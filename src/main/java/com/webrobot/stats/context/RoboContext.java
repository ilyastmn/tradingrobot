/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.webrobot.stats.model.Rate;

/**
 *
 * @author Ilyas
 */
public class RoboContext {

    //rates buffer
    //batch operations are available only for entities with the same
    //partition key. so, we need to store rates in Map<symbol, rates>
    private static Map<String, List<Rate>> ratesContainer;
    private static List<Map<String, List<Rate>>> archive;
    private static Map<String, Rate> latestRate;

    static {
        ratesContainer = Collections.synchronizedMap(new HashMap<String, List<Rate>>());
        archive = Collections.synchronizedList(new ArrayList<Map<String, List<Rate>>>());
        latestRate = Collections.synchronizedMap(new HashMap<String, Rate>());
    }

    public static Map<String, List<Rate>> getRatesContainer() {
        return ratesContainer;
    }

    public static Map<String, Rate> getLatestRate() {
        return latestRate;
    }

    public static void updateLatestRate(String late, Rate rate) throws NullPointerException{
        if(late==null || rate==null ) throw new NullPointerException();
        latestRate.put(late, rate);
     
    }

    public static List<Map<String, List<Rate>>> getArchive() {
        return archive;
    }

    public static Rate getLatestRateForSymbol(String symbol) {
        return latestRate.get(symbol);
    }

    public static synchronized void archiveRates() throws NullPointerException{
        
        archive.add(new HashMap<String, List<Rate>>(ratesContainer));
        MarkovContext.updateMarkovCache(new HashMap<String, List<Rate>>(ratesContainer));
        ratesContainer.clear();
    }

    public static void addRateToContainer(Rate r) {
        if (ratesContainer.containsKey(r.getSymbol())) {
            ((List<Rate>) ratesContainer.get(r.getSymbol())).add(r);
        } else {
            ratesContainer.put(r.getSymbol(), new ArrayList<Rate>(Arrays.asList(r)));
        }
    }

    public static synchronized void flush() {
        archive.clear();
    }
}
