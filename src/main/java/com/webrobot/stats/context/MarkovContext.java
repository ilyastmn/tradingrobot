/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import com.webrobot.stats.hmm.Occurence;
import com.webrobot.stats.hmm.Prediction;
import com.webrobot.stats.model.Rate;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author kilrwhle
 */
public class MarkovContext {

    public static long iterations = 0L;
    private static Map<String, List<Rate>> markovCache;
    private static Map<String, Set<Occurence>> occurences;
    private static Map<String, Prediction> predictions;

    static {
        markovCache = Collections.synchronizedMap(new HashMap<String, List<Rate>>());
        occurences = Collections.synchronizedMap(new HashMap<String, Set<Occurence>>());
        predictions = Collections.synchronizedMap(new HashMap<String, Prediction>());
    }

    public static synchronized void resetOccurences() {
        occurences.clear();
    }

    public static synchronized void resetCache() {
        markovCache.clear();
    }

    public static synchronized void resetPredictions() {
        predictions.clear();
    }

    public static Map<String, Prediction> getPredictions() {
        return predictions;
    }

    public static Map<String, Set<Occurence>> getOccurences() {
        return occurences;
    }

    public static Set<Occurence> getOccurencesForSymbol(String symbol) {
        return occurences.get(symbol);
    }

    public static List<Rate> getRatesForSymbol(String symbol) {
        return markovCache.get(symbol);
    }

    public static synchronized void updateMarkovCache(Map<String, List<Rate>> newData) {
        markovCache.putAll(newData);
    }

    public static synchronized Map<String, List<Rate>> getMarkovCache() {
        return markovCache;
    }

    /**
     * Calculates percentage of trust for prediction on one symbol
     *
     * @param symbol
     * @return
     */
    public static synchronized double getReliabilityForSymbol(String symbol) {
        if (predictions.containsKey(symbol)) {
            return predictions.get(symbol).getHitCount() / iterations;
        } else {
            return 0D;
        }
    }

    public static synchronized Prediction getPredictionBySymbol(String symbol) {
        return predictions.get(symbol);
    }

    public static synchronized void updatePrediction(String symbol, double price) {
        if (predictions.containsKey(symbol)) {
            predictions.get(symbol).setPrice(price);
        } else {
            predictions.put(symbol, new Prediction(symbol, price));
        }
    }

    /**
     * Increment occurences count for particular stock or add new values
     *
     * @param symbol
     * @param last
     * @param current
     */
    public static synchronized void pushValue(String symbol, double last, double current) {
        if (!occurences.containsKey(symbol)) {
            occurences.put(symbol, new HashSet<Occurence>());
        }

        Occurence candidate = getCandidate(symbol, last);

        if (candidate == null) {
            occurences.get(symbol).add(new Occurence(last, current, symbol));
        } else {
            candidate.increment();
        }
    }

    /**
     * Finds occurence with the same or similar previous price value
     *
     * @param symbol
     * @param price
     * @return
     */
    public static synchronized Occurence getCandidate(String symbol, double price) {
        Occurence result = null;
        Set<Occurence> occurences = getOccurencesForSymbol(symbol);
        for (Occurence o : occurences) {
            if (o.isInFuzzyRange(price)) {
                result = o;
                break;
            }
        }
        return result;
    }

    /**
     * Finds occurence with the price closest to specified
     *
     * @param symbol
     * @param price
     * @return
     */
    public static synchronized Occurence getCandidateClosest(String symbol, double price) {
        Occurence result = null;
        double diff = Double.MAX_VALUE;
        double comp = 0D;
        Set<Occurence> occurences = getOccurencesForSymbol(symbol);
        for (Occurence o : occurences) {
            comp = Math.abs(o.getPreviousPrice() - price);
            if (comp < diff) {
                diff = comp;
                result = o;
            }
        }
        return result;
    }
}
