/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import com.webrobot.stats.model.Rate;
import com.webrobot.stats.model.Stock;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ilyas
 */
public class FinanceContext {

    public static double wallet = 10000;
    public static Map<String, List<Stock>> boughtStock;

    static {
        boughtStock = new HashMap<String, List<Stock>>();
    }

    public static void buyRate(Rate rate, int amount) throws Exception {
        if (wallet < 0) {
            throw new VerifyError("WALLET IS EMPTY");
        }
        Stock s = new Stock(rate, amount);
        if (!boughtStock.containsKey(rate.getSymbol())) {
            List<Stock> rates = new ArrayList<>();
            rates.add(s);
            boughtStock.put(rate.getSymbol(), rates);
        } else {
            boughtStock.get(rate.getSymbol()).add(s);
        }

        wallet = wallet - s.getPrice();
    }

    public static void tryToSellRate(Rate rateToAnalyze) throws NullPointerException{
        if(rateToAnalyze==null){
            throw new NullPointerException("Rate is null");
        }
        List<Stock> rates = boughtStock.get(rateToAnalyze.getSymbol());

        Iterator<Stock> it = rates.iterator();
        while (it.hasNext()) {
            Stock s = it.next();
            if ((s.getRate().getBid() < rateToAnalyze.getAsk())) {
                System.out.println("SOLD");
                wallet += rateToAnalyze.getAsk() * s.getAmount();
                it.remove();
            }
        }
    }
    
    
}
