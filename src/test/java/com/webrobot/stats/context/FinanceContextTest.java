/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import com.webrobot.stats.model.Rate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author falcon
 */
public class FinanceContextTest {
    
    public FinanceContextTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of buyRate method, of class FinanceContext.
     */
    @org.junit.Test
    public void testBuyZeroRate() throws Exception {
        System.out.println("buyRate");
        Rate rate = new Rate("test");
        int amount = 0;
        double f = FinanceContext.wallet;
        FinanceContext.buyRate(rate, amount);
        assertEquals(f, FinanceContext.wallet, 0);
    }
    
    @org.junit.Test
    public void testBuyRate() throws Exception {
        System.out.println("buyRate");
        Rate rate = new Rate("n");
        int amount = 1;
        double f = FinanceContext.wallet - (amount * rate.getBid());
        FinanceContext.buyRate(rate, amount);
        assertEquals(f, FinanceContext.wallet, 0);
    }
    
    @org.junit.Test
    public void testSellRate() throws Exception {
        System.out.println("sellRate");
        Rate rate = new Rate("n");
        int amount = 1;
        FinanceContext.buyRate(rate, amount);
        rate.setAsk(50);
        FinanceContext.tryToSellRate(rate);
        assertEquals(0, FinanceContext.boughtStock.get(rate.getSymbol()).size());
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() {
        Rate rateToAnalyze = null;
        FinanceContext.tryToSellRate(rateToAnalyze);
    }
    
}
