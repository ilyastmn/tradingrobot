/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.context;

import com.webrobot.stats.model.KalmanWrapper;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author falcon
 */
public class KalmanContextTest {

    public KalmanContextTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testInsertionMethod() {
        Map<String, KalmanWrapper> wrapper = KalmanContext.filteredRates;
        wrapper.put("1", new KalmanWrapper());
        assertNotNull("not null", wrapper.get("1"));
    }

    @Test
    public void testDeletionMethod() {
        Map<String, KalmanWrapper> wrapper = KalmanContext.filteredRates;
        wrapper.put("1", new KalmanWrapper());
        wrapper.clear();
        assertTrue(wrapper.isEmpty());
    }
    
      @Test
    public void testnMethod() {
        Map<String, KalmanWrapper> wrapper = KalmanContext.filteredRates;
        wrapper.put("1", new KalmanWrapper());
        assertNull("null", wrapper.get("2"));
    }
    
}
