/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.webrobot.stats.context;

import com.webrobot.stats.model.Rate;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author falcon
 */
public class RoboContextTest {
    
    public RoboContextTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRatesContainer method, of class RoboContext.
     */
    @Test
    public void testGetRatesContainer() {
        System.out.println("getRatesContainer");
        
        Map<String, List<Rate>> result = RoboContext.getRatesContainer();
        assertNotNull(result);
     
    }

    /**
     * Test of getLatestRate method, of class RoboContext.
     */
    @Test
    public void testGetLatestRate() {
        System.out.println("getLatestRate");

        Map<String, Rate> result = RoboContext.getLatestRate();
        assertNotNull(result);
       
    }

    /**
     * Test of updateLatestRate method, of class RoboContext.
     */
    @Test(expected = Exception.class)
    public void testUpdateLatestRate() {
        System.out.println("updateLatestRate");
        String late = null;
        Rate rate = new Rate("n");
        RoboContext.updateLatestRate(late, rate);
    }

    /**
     * Test of getArchive method, of class RoboContext.
     */
    @Test
    public void testGetArchive() {
        System.out.println("getArchive");
       
        List<Map<String, List<Rate>>> result = RoboContext.getArchive();
        assertNotNull(result);
       
    }

    /**
     * Test of getLatestRateForSymbol method, of class RoboContext.
     */
    @Test
    public void testGetLatestRateForSymbol() {
        System.out.println("getLatestRateForSymbol");
        String symbol = "";
        Rate expResult = null;
        Rate result = RoboContext.getLatestRateForSymbol(symbol);
        assertNull( result);
 
    }

    /**
     * Test of archiveRates method, of class RoboContext.
     */
    @Test(timeout = 50)
    public void testArchiveRates() {
        System.out.println("archiveRates");
        RoboContext.archiveRates();
        
    }

  
}
