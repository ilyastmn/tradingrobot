/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webrobot.stats.worker;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author falcon
 */
public class WorkerLocatorTest {

    public WorkerLocatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getWorkerInstance method, of class WorkerLocator.
     */
    @Test
    public void testGetWorkerInstance() {
        System.out.println("getWorkerInstance");
        Worker result = WorkerLocator.getWorkerInstance();
        assertNotNull(result);

    }

    /**
     * Test of getFlusherInstance method, of class WorkerLocator.
     */
    @Test
    public void testGetFlusherInstance() {
        System.out.println("getFlusherInstance");

        Worker result = WorkerLocator.getFlusherInstance();
        assertNotNull(result);

    }

    /**
     * Test of getMarkovInstance method, of class WorkerLocator.
     */
    @Test
    public void testGetMarkovInstance() {
        System.out.println("getMarkovInstance");
        Worker expResult = null;
        Worker result = WorkerLocator.getMarkovInstance();
        assertNotNull(result);

    }

    /**
     * Test of getKalmanInstance method, of class WorkerLocator.
     */
    @Test
    public void testGetKalmanInstance() {
        System.out.println("getKalmanInstance");

        Worker result = WorkerLocator.getKalmanInstance();
        assertNotNull(result);

    }

}
